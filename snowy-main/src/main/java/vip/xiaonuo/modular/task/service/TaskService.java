/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.task.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.task.entity.Task;
import vip.xiaonuo.modular.task.param.TaskParam;
import vip.xiaonuo.modular.task.result.TaskResult;

import java.util.List;

/**
 * 任务service接口
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
public interface TaskService extends IService<Task> {

    /**
     * 查询任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    PageResult<TaskResult> page(TaskParam taskParam);

    /**
     * 任务列表
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    List<TaskResult> list(TaskParam taskParam);

    /**
     * 添加任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    void add(TaskParam taskParam);

    /**
     * 删除任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    void delete(List<TaskParam> taskParamList);

    /**
     * 编辑任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
    void edit(TaskParam taskParam);

    /**
     * 查看任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
     Task detail(TaskParam taskParam);

    /**
     * 导出任务
     *
     * @author bwl
     * @date 2022-06-02 09:27:11
     */
     void export(TaskParam taskParam);

    /**
     * 开启任务
     *
     * @author bwl
     * @date 2022-06-24 11:39:11
     */
    void start(List<TaskParam> taskParamList);
    /**
     * 结束任务
     *
     * @author bwl
     * @date 2022-06-24 11:39:11
     */
    void end(List<TaskParam> taskParamList);
}
