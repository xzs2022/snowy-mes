import { axios } from '@/utils/request'

/**
 * 查询客户分类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortPage (parameter) {
  return axios({
    url: '/cusSort/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 客户分类列表
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortList (parameter) {
  return axios({
    url: '/cusSort/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加客户分类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortAdd (parameter) {
  return axios({
    url: '/cusSort/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑客户分类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortEdit (parameter) {
  return axios({
    url: '/cusSort/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除客户分类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortDelete (parameter) {
  return axios({
    url: '/cusSort/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出客户分类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
export function cusSortExport (parameter) {
  return axios({
    url: '/cusSort/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 获取类型树
 *
 * @author czw
 * @date 2022-07-23 14:01:44
 */
export function getCusTree (parameter) {
  return axios({
    url: '/cusSort/tree',
    method: 'get',
    params: parameter
  })
}
