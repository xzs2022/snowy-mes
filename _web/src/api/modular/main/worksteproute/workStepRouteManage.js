import { axios } from '@/utils/request'

/**
 * 查询工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRoutePage (parameter) {
  return axios({
    url: '/workStepRoute/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工序路线关系表列表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRouteList (parameter) {
  return axios({
    url: '/workStepRoute/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRouteAdd (parameter) {
  return axios({
    url: '/workStepRoute/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRouteEdit (parameter) {
  return axios({
    url: '/workStepRoute/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRouteDelete (parameter) {
  return axios({
    url: '/workStepRoute/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出工序路线关系表
 *
 * @author 楊楊
 * @date 2022-05-27 15:12:44
 */
export function workStepRouteExport (parameter) {
  return axios({
    url: '/workStepRoute/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
