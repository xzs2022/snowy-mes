/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.promodelstep.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.promodelstep.param.ProModelStepParam;
import vip.xiaonuo.modular.promodelstep.service.ProModelStepService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 产品型号步骤控制器
 *
 * @author qinencheng
 * @date 2022-05-20 16:10:05
 */
@RestController
public class ProModelStepController {

    @Resource
    private ProModelStepService proModelStepService;

    /**
     * 查询产品型号步骤
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @GetMapping("/proModelStep/page")
    @BusinessLog(title = "产品型号步骤_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(ProModelStepParam proModelStepParam) {
        return new SuccessResponseData(proModelStepService.page(proModelStepParam));
    }

    /**
     * 添加产品型号步骤
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @PostMapping("/proModelStep/add")
    @BusinessLog(title = "产品型号步骤_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(ProModelStepParam.add.class) ProModelStepParam proModelStepParam) {
            proModelStepService.add(proModelStepParam);
        return new SuccessResponseData();
    }

    /**
     * 删除产品型号步骤，可批量删除
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @PostMapping("/proModelStep/delete")
    @BusinessLog(title = "产品型号步骤_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(ProModelStepParam.delete.class) List<ProModelStepParam> proModelStepParamList) {
            proModelStepService.delete(proModelStepParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑产品型号步骤
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @PostMapping("/proModelStep/edit")
    @BusinessLog(title = "产品型号步骤_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(ProModelStepParam.edit.class) ProModelStepParam proModelStepParam) {
            proModelStepService.edit(proModelStepParam);
        return new SuccessResponseData();
    }

    /**
     * 查看产品型号步骤
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @GetMapping("/proModelStep/detail")
    @BusinessLog(title = "产品型号步骤_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(ProModelStepParam.detail.class) ProModelStepParam proModelStepParam) {
        return new SuccessResponseData(proModelStepService.detail(proModelStepParam));
    }

    /**
     * 产品型号步骤列表
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @GetMapping("/proModelStep/list")
    @BusinessLog(title = "产品型号步骤_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(ProModelStepParam proModelStepParam) {
        return new SuccessResponseData(proModelStepService.list(proModelStepParam));
    }

    /**
     * 导出系统用户
     *
     * @author qinencheng
     * @date 2022-05-20 16:10:05
     */
    @Permission
    @GetMapping("/proModelStep/export")
    @BusinessLog(title = "产品型号步骤_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(ProModelStepParam proModelStepParam) {
        proModelStepService.export(proModelStepParam);
    }

}
