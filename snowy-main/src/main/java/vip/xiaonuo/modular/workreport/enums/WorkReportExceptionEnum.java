/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.enums;

import vip.xiaonuo.core.annotion.ExpEnumType;
import vip.xiaonuo.core.exception.enums.abs.AbstractBaseExceptionEnum;
import vip.xiaonuo.core.factory.ExpEnumCodeFactory;
import vip.xiaonuo.sys.core.consts.SysExpEnumConstant;

/**
 * 报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
@ExpEnumType(module = SysExpEnumConstant.SNOWY_SYS_MODULE_EXP_CODE)
public enum WorkReportExceptionEnum implements AbstractBaseExceptionEnum {

    /**
     * 良品数大于上次任务良品数
     */
    MORE_GOODNUM(4,"当前良品数不可以大于上次任务良品数"),
    /**
     * 报工数量不可以为负
     */
    NOT_ALLOWED(5,"报工数量不可以为负或为空"),
    /**
     * 良品数小于上次任务良品数
     */
    MIN_GOODNUM(6,"当前良品数不可以小于下次任务良品数"),

    /*
     * 任务状态不存在
     * /
     */
    NULL_TASK_STATUS (7,"任务状态不存在,请检查任务状态"),
    /**
     * 任务已结束
     */
    END_TASK(3,"任务状态已结束,不可以报工(任务状态执行中才可以报工)"),
    /**
     * 任务未开始
     */
    NOT_START_TASK(8,"任务状态未开始,不可以报工(任务状态执行中才可以报工)"),
    /**
     * 任务顺序不存在
     */
    NOT_TASK_SORNUMS(9,"任务顺序不存在,请检查当前任务顺序"),
    /**
     * 任务顺序重复
     */
    MORE_TASK_SORNUMS(10,"任务顺序重复,请检查当前任务顺序"),
    /*
     * 工单状态不存在
     * /
     */
    NULL_ORDER_STATUS (11,"工单状态不存在,请检查任务状态"),
    /**
     * 工单未开始
     */
    NOT_START_ORDER(12,"工单状态未开始,不可以报工(工单状态执行中才可以报工)"),
    /**
     * 工单已结束
     */
    END_ORDER(13,"工单状态已结束,不可以报工(工单状态执行中才可以报工)"),

    /**
     * 工单已取消
     */
    CANCELED_ORDER(14,"工单状态已取消,不可以报工(工单状态执行中才可以报工)"),
    /**
     * 重复审批
     */
    APPROVAL_MORE(15,"审批已通过,请勿重复审批"),
    /**
     * 报工开始时间不能大于等于结束时间
     */
    REPORT_START_END_TIME(16,"报工开始时间不能大于等于结束时间"),
    /**
     *
     * 数据不存在
     */
    NOT_EXIST(1, "此数据不存在");

    private final Integer code;

    private final String message;
    WorkReportExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return ExpEnumCodeFactory.getExpEnumCode(this.getClass(), code);
    }

    @Override
    public String getMessage() {
        return message;
    }

}
