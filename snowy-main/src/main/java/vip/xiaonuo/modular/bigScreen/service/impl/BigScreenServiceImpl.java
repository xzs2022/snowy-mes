package vip.xiaonuo.modular.bigScreen.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tencentcloudapi.ame.v20190916.models.Item;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import vip.xiaonuo.modular.bigScreen.mapper.BigScreenMapper;
import vip.xiaonuo.modular.bigScreen.param.WorkStepOrderParam;
import vip.xiaonuo.modular.bigScreen.result.*;
import vip.xiaonuo.modular.bigScreen.service.BigScreenService;
import vip.xiaonuo.modular.invOut.entity.InvOut;
import vip.xiaonuo.modular.task.entity.Task;
import vip.xiaonuo.modular.task.enums.TaskStatusEnum;
import vip.xiaonuo.modular.task.service.TaskService;
import vip.xiaonuo.modular.workorder.entity.WorkOrder;
import vip.xiaonuo.modular.workorder.enums.WorkOrderStatusEnum;
import vip.xiaonuo.modular.workorder.service.WorkOrderService;
import vip.xiaonuo.modular.workreport.mapper.WorkReportMapper;
import vip.xiaonuo.modular.workreport.service.WorkReportService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class BigScreenServiceImpl implements BigScreenService {
    @Resource
    private WorkReportService workReportService;
    @Resource
    private BigScreenMapper bigScreenMapper;
    @Resource
    private WorkOrderService workOrderService;
    @Resource
    private TaskService taskService;

    //查询近10天每天不良品率和数量
    @Override
    public List<RejectsResult> queryRejects() {
        List<RejectsResult> rejectsResult= bigScreenMapper.selectRejects();
        rejectsResult.forEach(item->{
            if (ObjectUtil.isEmpty(item.getRejectsNum()))
            {
                item.setRejectsNum(0);
            }
            if (ObjectUtil.isEmpty(item.getRejectsRate()))
            {
                item.setRejectsRate(0.00d);
            }
        });
        return rejectsResult;
    }
    //查询近10天每天工单产出数量和完成数量
    @Override
    public List<WorkOrderProResult> queryWorkOrderPro() {
        List<WorkOrderProResult> workOrderProResult =bigScreenMapper.selectWorkOrderPro();
        workOrderProResult.forEach(item->{
            if (ObjectUtil.isEmpty(item.getWorkOrderFinishNum()))
            {
                item.setWorkOrderFinishNum(0);
            }
            if (ObjectUtil.isEmpty(item.getWorkOrderProNum()))
            {
                item.setWorkOrderProNum(0);
            }
        });
        return workOrderProResult;
    }
    //大屏中间部分（在制工单数，在制任务数。。。。。。。）
    @Override
    public TodayAndConductResult todayAndConduct() {
        TodayAndConductResult todayAndConductResult=new TodayAndConductResult();
        //在制工单数量
        QueryWrapper<WorkOrder> workOrderQueryWrapper = new QueryWrapper<>();
        workOrderQueryWrapper.lambda().eq(WorkOrder::getStatus, WorkOrderStatusEnum.EXECUTION);
        Integer conductWorkOrderCount= workOrderService.count(workOrderQueryWrapper);
        todayAndConductResult.setConductWorkOrderCount(conductWorkOrderCount);
        //延期工单数
        workOrderQueryWrapper.lambda().lt(WorkOrder::getPlaEndTime,new Date());
        Integer delayWorkOrderCount= workOrderService.count(workOrderQueryWrapper);
        todayAndConductResult.setDelayWorkOrderCount(delayWorkOrderCount);
        //在制任务数
        QueryWrapper<Task> TaskQueryWrapper = new QueryWrapper<>();
        TaskQueryWrapper.lambda().eq(Task::getStatus, TaskStatusEnum.EXECUTION);
        Integer conductTaskCount= taskService.count(TaskQueryWrapper);
        todayAndConductResult.setConductTaskCount(conductTaskCount);
        //延期任务数
        TaskQueryWrapper.lambda().lt(Task::getPlaEndTime,new Date());
        Integer delayTaskCount= taskService.count(TaskQueryWrapper);
        todayAndConductResult.setDelayTaskCount(delayTaskCount);
        //今日不良品率
        List<RejectsResult> rejectsResult=this.queryRejects();
        Double rejectsRate=rejectsResult.get(rejectsResult.size()-1).getRejectsRate();
        todayAndConductResult.setRejectsRate(rejectsRate);
        //今日完成工单今日产出工单
        List<WorkOrderProResult> workOrderProResults = queryWorkOrderPro();
        Integer workOrderFinishNum=workOrderProResults.get(rejectsResult.size()-1).getWorkOrderFinishNum();
        Integer workOrderProNum=workOrderProResults.get(rejectsResult.size()-1).getWorkOrderProNum();
        todayAndConductResult.setWorkOrderFinishNum(workOrderFinishNum);
        todayAndConductResult.setWorkOrderProNum(workOrderProNum);
        //今日完成任务
        QueryWrapper<Task> TaskQueryWrapper1 = new QueryWrapper<>();
        TaskQueryWrapper1.apply("date_format(fact_end_time,'%Y-%m-%d')=CURDATE()");
        Integer taskFinishCount= taskService.count(TaskQueryWrapper1);
        todayAndConductResult.setTaskFinishCount(taskFinishCount);
        //在制任务计划数
        QueryWrapper<Task> TaskQueryWrapper2 = new QueryWrapper<>();
        TaskQueryWrapper2.select("sum(pla_num) as pla_num");
        TaskQueryWrapper2.lambda().eq(Task::getStatus, WorkOrderStatusEnum.EXECUTION);
        Task task= taskService.getOne(TaskQueryWrapper2);
        if (ObjectUtil.isNotEmpty(task)&&ObjectUtil.isNotEmpty(task.getPlaNum()))
        {
            Integer taskPalNum=task.getPlaNum();
            todayAndConductResult.setTaskPalNum(taskPalNum);
        }else {
            todayAndConductResult.setTaskPalNum(0);
        }


        //在制工单计划数
        QueryWrapper<WorkOrder> WorkOrderQueryWrapper1 = new QueryWrapper<>();
        WorkOrderQueryWrapper1.select("sum(pla_num) as pla_num");
        WorkOrderQueryWrapper1.lambda().eq(WorkOrder::getStatus, WorkOrderStatusEnum.EXECUTION);
        WorkOrder workOrder=workOrderService.getOne(WorkOrderQueryWrapper1);
        if (ObjectUtil.isNotEmpty(workOrder)&&ObjectUtil.isNotEmpty(workOrder.getPlaNum()))
        {
            Integer workOrderPalNum=task.getPlaNum();
            todayAndConductResult.setWorkOrderPalNum(workOrderPalNum);
        }else {
            todayAndConductResult.setWorkOrderPalNum(0);
        }


        return todayAndConductResult;
    }
    //工单预警（剩余天数）
    @Override
    public List<WorkOrderAlertResult> queryWorkOrderAlert() {
        List<WorkOrderAlertResult> workOrderAlertResults=bigScreenMapper.selectWorkOrderAlert();
        return workOrderAlertResults;
    }
    //工序任务（每个工序对应任务的统计）
    @Override
    public List<WorkStepOrderResult> queryWorkStepOrder(WorkStepOrderParam workStepOrderParam) {
        List<WorkStepOrderResult> workStepOrderResults =bigScreenMapper.selectWorkStepOrder(workStepOrderParam);
        workStepOrderResults.forEach(item->{
            if (ObjectUtil.isEmpty(item.getBadNum()))
            {
                item.setBadNum(0);
            }
            if (ObjectUtil.isEmpty(item.getGoodNum()))
            {
                item.setGoodNum(0);
            }
            if (ObjectUtil.isEmpty(item.getPlannedSpeed()))
            {
                item.setPlannedSpeed(0.00d);
            }
            if (ObjectUtil.isEmpty(item.getTaskNum()))
            {
                item.setTaskNum(0);
            }
            if (ObjectUtil.isEmpty(item.getDefectRate()))
            {
                item.setDefectRate(0.00d);
            }

        });
        return workStepOrderResults;
    }


}
