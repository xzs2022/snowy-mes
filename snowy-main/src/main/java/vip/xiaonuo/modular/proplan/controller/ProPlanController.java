/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.proplan.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.proplan.param.ProPlanParam;
import vip.xiaonuo.modular.proplan.service.ProPlanService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 生产计划控制器
 *
 * @author 嘉欣
 * @date 2022-07-19 11:40:28
 */
@RestController
public class ProPlanController {

    @Resource
    private ProPlanService proPlanService;

    /**
     * 查询生产计划
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @GetMapping("/proPlan/page")
    @BusinessLog(title = "生产计划_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(ProPlanParam proPlanParam) {
        return new SuccessResponseData(proPlanService.page(proPlanParam));
    }

    /**
     * 添加生产计划
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @PostMapping("/proPlan/add")
    @BusinessLog(title = "生产计划_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(ProPlanParam.add.class) ProPlanParam proPlanParam) {
            proPlanService.add(proPlanParam);
        return new SuccessResponseData();
    }

    /**
     * 删除生产计划，可批量删除
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @PostMapping("/proPlan/delete")
    @BusinessLog(title = "生产计划_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(ProPlanParam.delete.class) List<ProPlanParam> proPlanParamList) {
            proPlanService.delete(proPlanParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑生产计划
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @PostMapping("/proPlan/edit")
    @BusinessLog(title = "生产计划_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(ProPlanParam.edit.class) ProPlanParam proPlanParam) {
            proPlanService.edit(proPlanParam);
        return new SuccessResponseData();
    }

    /**
     * 查看生产计划
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @GetMapping("/proPlan/detail")
    @BusinessLog(title = "生产计划_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(ProPlanParam.detail.class) ProPlanParam proPlanParam) {
        return new SuccessResponseData(proPlanService.detail(proPlanParam));
    }

    /**
     * 生产计划列表
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @GetMapping("/proPlan/list")
    @BusinessLog(title = "生产计划_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(ProPlanParam proPlanParam) {
        return new SuccessResponseData(proPlanService.list(proPlanParam));
    }

    /**
     * 导出系统用户
     *
     * @author 嘉欣
     * @date 2022-07-19 11:40:28
     */
    @Permission
    @GetMapping("/proPlan/export")
    @BusinessLog(title = "生产计划_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(ProPlanParam proPlanParam) {
        proPlanService.export(proPlanParam);
    }

}
