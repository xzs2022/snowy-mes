import { axios } from '@/utils/request'

/**
 * 查询工序
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepPage (parameter) {
  return axios({
    url: '/workStep/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工序列表
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepList (parameter) {
  return axios({
    url: '/workStep/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工序
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepAdd (parameter) {
  return axios({
    url: '/workStep/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工序
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepEdit (parameter) {
  return axios({
    url: '/workStep/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工序
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepDelete (parameter) {
  return axios({
    url: '/workStep/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出工序
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
export function workStepExport (parameter) {
  return axios({
    url: '/workStep/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
