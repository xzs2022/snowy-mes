/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.*;
import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 报工表
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("dw_work_report")
public class WorkReport extends BaseEntity {
    /**
     * 报工表主键Id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 工单id
     */
    @Excel(name = "工单id")
    private Long workOrderId;

    /**
     * 任务id
     */
    @Excel(name = "任务id")
    private Long taskId;
    /**
     * 工序状态
     */
//    @Excel(name = "工序状态")
//    private Integer stepStatus;

    /**
     * 生产人员
     */
    @Excel(name = "生产人员")
    private Long productionUser;

    /**
     * 报工数
     */
    @Excel(name = "报工数")
    private Integer reportNum;
    /**
     * 良品数
     */
    @Excel(name = "良品数")
    private Integer goodNum;
    /**
     * 不良品数
     */
    @Excel(name = "不良品数")
    private Integer badNum;
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "开始时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd HH:mm", width = 20)
    private Date startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "结束时间", databaseFormat = "yyyy-MM-dd HH:mm", format = "yyyy-MM-dd", width = 20)
    private Date endTime;

    /**
     * 工作时间
     */
    @Excel(name ="工作时长")
    private Integer workTime;
    //区分从何处添加
    @Excel(name ="区分从何处添加")
    private Integer   distinction;

    /**
     * 不良品项
     */
//    @Excel(name = "不良品项")
//    private String mulBadItem;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remarks;
/*
*  是否审批
 */

    @Excel(name = "审批")
    private Integer approval;
}
