/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workroute.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.workroute.param.WorkRouteParam;
import vip.xiaonuo.modular.workroute.service.WorkRouteService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工艺路线控制器
 *
 * @author bwl
 * @date 2022-05-24 15:33:25
 */
@RestController
public class WorkRouteController {

    @Resource
    private WorkRouteService workRouteService;

    /**
     * 查询工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @GetMapping("/workRoute/page")
    @BusinessLog(title = "工艺路线_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkRouteParam workRouteParam) {
        return new SuccessResponseData(workRouteService.page(workRouteParam));
    }

    /**
     * 添加工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @PostMapping("/workRoute/add")
    @BusinessLog(title = "工艺路线_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkRouteParam.add.class) WorkRouteParam workRouteParam) {
            workRouteService.add(workRouteParam);
        return new SuccessResponseData();
    }

    /**
     * 删除工艺路线，可批量删除
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @PostMapping("/workRoute/delete")
    @BusinessLog(title = "工艺路线_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkRouteParam.delete.class) List<WorkRouteParam> workRouteParamList) {
            workRouteService.delete(workRouteParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @PostMapping("/workRoute/edit")
    @BusinessLog(title = "工艺路线_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkRouteParam.edit.class) WorkRouteParam workRouteParam) {
            workRouteService.edit(workRouteParam);
        return new SuccessResponseData();
    }

    /**
     * 查看工艺路线
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @GetMapping("/workRoute/detail")
    @BusinessLog(title = "工艺路线_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkRouteParam.detail.class) WorkRouteParam workRouteParam) {
        return new SuccessResponseData(workRouteService.detail(workRouteParam));
    }

    /**
     * 工艺路线列表
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @GetMapping("/workRoute/list")
    @BusinessLog(title = "工艺路线_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkRouteParam workRouteParam) {
        return new SuccessResponseData(workRouteService.list(workRouteParam));
    }

    /**
     * 导出系统用户
     *
     * @author bwl
     * @date 2022-05-24 15:33:25
     */
    @Permission
    @GetMapping("/workRoute/export")
    @BusinessLog(title = "工艺路线_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkRouteParam workRouteParam) {
        workRouteService.export(workRouteParam);
    }

}
