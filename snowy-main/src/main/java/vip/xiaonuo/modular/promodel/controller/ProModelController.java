/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.promodel.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.promodel.param.ProModelParam;
import vip.xiaonuo.modular.promodel.service.ProModelService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import vip.xiaonuo.modular.protype.service.ProTypeService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 产品型号表控制器
 *
 * @author wz
 * @date 2022-05-25 10:08:07
 */
@RestController
public class ProModelController {

    @Resource
    private ProModelService proModelService;


    /**
     * 查询产品型号表
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @GetMapping("/proModel/page")
    @BusinessLog(title = "产品型号表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(ProModelParam proModelParam) {
        return new SuccessResponseData(proModelService.page(proModelParam));
    }

    /**
     * 添加产品型号表
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @PostMapping("/proModel/add")
    @BusinessLog(title = "产品型号表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(ProModelParam.add.class) ProModelParam proModelParam) {
            proModelService.add(proModelParam);
        return new SuccessResponseData();
    }

    /**
     * 删除产品型号表，可批量删除
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @PostMapping("/proModel/delete")
    @BusinessLog(title = "产品型号表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(ProModelParam.delete.class) List<ProModelParam> proModelParamList) {
            proModelService.delete(proModelParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑产品型号表
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @PostMapping("/proModel/edit")
    @BusinessLog(title = "产品型号表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(ProModelParam.edit.class) ProModelParam proModelParam) {
            proModelService.edit(proModelParam);
        return new SuccessResponseData();
    }

    /**
     * 查看产品型号表
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @GetMapping("/proModel/detail")
    @BusinessLog(title = "产品型号表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(ProModelParam.detail.class) ProModelParam proModelParam) {
        return new SuccessResponseData(proModelService.detail(proModelParam));
    }

    /**
     * 产品型号表列表
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @GetMapping("/proModel/list")
    @BusinessLog(title = "产品型号表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(ProModelParam proModelParam) {
        return new SuccessResponseData(proModelService.list(proModelParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-05-25 10:08:07
     */
    @Permission
    @GetMapping("/proModel/export")
    @BusinessLog(title = "产品型号表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(ProModelParam proModelParam) {
        proModelService.export(proModelParam);
    }

}
