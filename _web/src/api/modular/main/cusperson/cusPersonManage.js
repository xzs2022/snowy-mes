import { axios } from '@/utils/request'

/**
 * 查询客户联系人
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonPage (parameter) {
  return axios({
    url: '/cusPerson/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 客户联系人列表
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonList (parameter) {
  return axios({
    url: '/cusPerson/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加客户联系人
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonAdd (parameter) {
  return axios({
    url: '/cusPerson/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑客户联系人
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonEdit (parameter) {
  return axios({
    url: '/cusPerson/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除客户联系人
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonDelete (parameter) {
  return axios({
    url: '/cusPerson/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出客户联系人
 *
 * @author czw
 * @date 2022-07-20 12:28:28
 */
export function cusPersonExport (parameter) {
  return axios({
    url: '/cusPerson/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
