/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppperson.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.suppperson.param.SuppPersonParam;
import vip.xiaonuo.modular.suppperson.service.SuppPersonService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 供应商联系人控制器
 *
 * @author zjk
 * @date 2022-08-04 13:41:10
 */
@RestController
public class SuppPersonController {

    @Resource
    private SuppPersonService suppPersonService;

    /**
     * 查询供应商联系人
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @GetMapping("/suppPerson/page")
    @BusinessLog(title = "供应商联系人_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(SuppPersonParam suppPersonParam) {
        return new SuccessResponseData(suppPersonService.page(suppPersonParam));
    }

    /**
     * 添加供应商联系人
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @PostMapping("/suppPerson/add")
    @BusinessLog(title = "供应商联系人_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(SuppPersonParam.add.class) SuppPersonParam suppPersonParam) {
            suppPersonService.add(suppPersonParam);
        return new SuccessResponseData();
    }

    /**
     * 删除供应商联系人，可批量删除
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @PostMapping("/suppPerson/delete")
    @BusinessLog(title = "供应商联系人_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(SuppPersonParam.delete.class) List<SuppPersonParam> suppPersonParamList) {
            suppPersonService.delete(suppPersonParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑供应商联系人
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @PostMapping("/suppPerson/edit")
    @BusinessLog(title = "供应商联系人_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(SuppPersonParam.edit.class) SuppPersonParam suppPersonParam) {
            suppPersonService.edit(suppPersonParam);
        return new SuccessResponseData();
    }

    /**
     * 查看供应商联系人
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @GetMapping("/suppPerson/detail")
    @BusinessLog(title = "供应商联系人_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(SuppPersonParam.detail.class) SuppPersonParam suppPersonParam) {
        return new SuccessResponseData(suppPersonService.detail(suppPersonParam));
    }

    /**
     * 供应商联系人列表
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @GetMapping("/suppPerson/list")
    @BusinessLog(title = "供应商联系人_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(SuppPersonParam suppPersonParam) {
        return new SuccessResponseData(suppPersonService.list(suppPersonParam));
    }

    /**
     * 导出系统用户
     *
     * @author zjk
     * @date 2022-08-04 13:41:10
     */
    @Permission
    @GetMapping("/suppPerson/export")
    @BusinessLog(title = "供应商联系人_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(SuppPersonParam suppPersonParam) {
        suppPersonService.export(suppPersonParam);
    }

}