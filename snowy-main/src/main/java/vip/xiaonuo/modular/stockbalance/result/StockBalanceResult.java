package vip.xiaonuo.modular.stockbalance.result;

import lombok.Data;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import vip.xiaonuo.modular.stockbalance.entity.StockBalance;


/**
 * @author 苏辰
 */
@Data
public class StockBalanceResult extends StockBalance {
    /**
     * 仓库名称
     */
    private String warHouName;
    /**
     * 产品名称
     */
    private String proName;
}
