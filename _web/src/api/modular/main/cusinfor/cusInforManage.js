import { axios } from '@/utils/request'

/**
 * 查询客户资料
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforPage (parameter) {
  return axios({
    url: '/cusInfor/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 客户资料列表
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforList (parameter) {
  return axios({
    url: '/cusInfor/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加客户资料
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforAdd (parameter) {
  return axios({
    url: '/cusInfor/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑客户资料
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforEdit (parameter) {
  return axios({
    url: '/cusInfor/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除客户资料
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforDelete (parameter) {
  return axios({
    url: '/cusInfor/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出客户资料
 *
 * @author czw
 * @date 2022-07-20 11:37:49
 */
export function cusInforExport (parameter) {
  return axios({
    url: '/cusInfor/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
